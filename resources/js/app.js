
require('./bootstrap');

window.Vue = require('vue');

//Editor
import 'v-markdown-editor/dist/v-markdown-editor.css';
import Editor from 'v-markdown-editor'
Vue.use(Editor);
//Editor End

//vue x
import Vuex from 'vuex'
Vue.use(Vuex)
import storeData from "./store/index"
const store = new Vuex.Store(
    storeData
)
//Vue x End



//Moment js
import {filter} from './filter'
//Moment js End



// vue router
import VueRouter from 'vue-router'
Vue .use(VueRouter)
// vue router End



import {routes} from "./routes";
// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// import ExampleComponent from './components/ExampleComponent.vue';
//  Vue.component('example-component', ExampleComponent);
import AdminMaster from './components/admin/AdminMaster';
 Vue.component('admin-main',AdminMaster);

import publicMaster from './components/public/publicMaster';
Vue.component('home-main',publicMaster);


//v-from
import { Form, HasError, AlertError } from 'vform'
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
window.Form = Form;
//v-from End



//sweet Alert
import swal from 'sweetalert2'
window.swal = swal;
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2500,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer)
        toast.addEventListener('mouseleave', swal.resumeTimer)
    }
})
window.toast = toast
//sweet Alert End




const router = new VueRouter({
    routes, // short for `routes: routes`
    mode:'hash'
})



const app = new Vue({
    el: '#app',
    router,
    store
});



