-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2020 at 08:06 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `superblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `created_at`, `updated_at`) VALUES
(1, 'Alfreda Veum', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(2, 'Selina Klocko', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(3, 'Nathaniel Mills', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(4, 'Lilly Considine', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(5, 'Destin Kris', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(6, 'Velma Kshlerin IV', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(7, 'Krista Witting', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(8, 'Grace Jakubowski', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(9, 'Mariana Deckow', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(10, 'Roslyn Dach', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(11, 'Prof. Verner D\'Amore', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(12, 'Sydni Wolff', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(13, 'Dr. Vernon Grant DVM', '2020-08-22 00:04:54', '2020-08-22 00:04:54'),
(14, 'Karine Shields', '2020-08-22 00:04:55', '2020-08-22 00:04:55'),
(15, 'Damion Williamson', '2020-08-22 00:04:55', '2020-08-22 00:04:55'),
(16, 'Ernest Maggio V', '2020-08-22 00:04:55', '2020-08-22 00:04:55'),
(17, 'Edison Parker', '2020-08-22 00:04:55', '2020-08-22 00:04:55'),
(18, 'Miss Elouise Grant', '2020-08-22 00:04:55', '2020-08-22 00:04:55'),
(19, 'Tess Volkman', '2020-08-22 00:04:55', '2020-08-22 00:04:55'),
(20, 'Lucas Rippin', '2020-08-22 00:04:55', '2020-08-22 00:04:55'),
(21, 'Prof. Beth Hayes IV', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(22, 'Prof. Alfreda Rau', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(23, 'Sibyl Cruickshank PhD', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(24, 'Kip Schimmel', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(25, 'Adan Mayer', '2020-08-22 00:04:56', '2020-08-22 00:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_07_18_140350_create_categories_table', 1),
(4, '2020_07_18_140807_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `comment_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `cat_id`, `user_id`, `comment_id`, `title`, `description`, `photo`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 5, 'Quis qui ipsum quo autem libero.', 'Consectetur suscipit dicta quisquam eum. Dolores quibusdam tempora iusto eveniet fuga corrupti fuga. Ad accusamus cum voluptas quasi quis ut.', 'https://lorempixel.com/640/480/?30616', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(2, 10, 9, 4, 'Neque sed quod adipisci error sed nisi eveniet.', 'Aliquam quidem doloremque pariatur at. Amet in a ab laborum excepturi. Rerum provident repellendus commodi ut. In vero molestiae vel vel voluptas quia enim.', 'https://lorempixel.com/640/480/?38123', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(3, 3, 3, 4, 'Doloribus quia nihil molestias.', 'Id et reprehenderit ea mollitia soluta. Omnis et corrupti facilis pariatur omnis. Illo qui numquam quaerat enim rerum repellendus dolore. Et natus ad officiis. Minima eaque dolorum omnis.', 'https://lorempixel.com/640/480/?56382', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(4, 4, 4, 9, 'Non et aliquam explicabo harum id illum rerum.', 'Porro molestiae reprehenderit recusandae cupiditate consectetur. Omnis ullam dignissimos adipisci et. Odio officiis voluptates laborum qui suscipit eos magni.', 'https://lorempixel.com/640/480/?59812', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(5, 7, 7, 9, 'Necessitatibus voluptatum quas omnis mollitia velit nemo distinctio.', 'Atque laudantium et minus quae quia. Sunt reiciendis maiores laborum repellendus enim voluptatem ut. Ea laudantium magni debitis consectetur dolorum ea autem. Voluptatum ut sequi et pariatur ea facilis similique et. Enim iste nobis veniam consequuntur et ipsum et voluptatibus.', 'https://lorempixel.com/640/480/?82760', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(6, 7, 5, 6, 'Ea voluptas occaecati similique tempore ut perferendis quos.', 'Aut odio ipsum provident voluptatibus sit nisi. Rerum fugiat blanditiis deserunt. Quibusdam qui consectetur optio iusto cum est consequatur. Et ut neque perspiciatis sed labore facilis odio.', 'https://lorempixel.com/640/480/?63766', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(7, 10, 2, 3, 'Assumenda sint maxime sint voluptatum quia quisquam.', 'Nulla cupiditate dolores praesentium asperiores et quod. Sint ut quasi aut incidunt. Mollitia nihil vel excepturi omnis.', 'https://lorempixel.com/640/480/?22731', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(8, 7, 6, 8, 'Rerum minima totam voluptas.', 'Atque aut sed occaecati earum deserunt. Atque aut enim non laborum tempora voluptatibus. Iusto velit aspernatur a veniam ratione recusandae. Dolor voluptas dolor nisi odio ipsam consequuntur ipsum.', 'https://lorempixel.com/640/480/?62447', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(9, 6, 4, 2, 'Et dicta adipisci consectetur dolores unde.', 'Ut reprehenderit ipsam velit provident beatae et sunt. Aut pariatur excepturi amet non. Eos nulla omnis laudantium alias veritatis et. Hic ducimus veritatis quaerat sint natus ratione.', 'https://lorempixel.com/640/480/?49747', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(10, 3, 7, 5, 'Modi doloribus maiores consectetur harum quas enim quia minus.', 'Eos qui sed deserunt labore maiores. Accusantium consequatur praesentium neque ut laborum cupiditate. Libero odit nemo corporis est omnis et. Ipsa itaque quo voluptatum nemo optio necessitatibus.', 'https://lorempixel.com/640/480/?36312', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(11, 10, 5, 4, 'Autem aliquid minus et sed.', 'Quidem temporibus deserunt earum itaque exercitationem animi sequi quaerat. Voluptatem ducimus adipisci ipsa maxime ea non. Quia minus est est.', 'https://lorempixel.com/640/480/?40835', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(12, 8, 2, 6, 'Nam quod rerum tempora optio suscipit.', 'Velit dignissimos provident voluptate in ut qui aut. Vel omnis molestiae sit odit nisi recusandae. Non odio magni deserunt atque numquam cumque. Veniam eum sunt corrupti non provident.', 'https://lorempixel.com/640/480/?55542', '2020-08-22 00:04:52', '2020-08-22 00:04:52'),
(13, 9, 3, 6, 'Fugit distinctio saepe quam dicta.', 'Nostrum doloribus ut dolore exercitationem est sunt. Et quae est ducimus deserunt. Est fuga eius est adipisci numquam voluptas nulla. Unde sint ut repudiandae omnis aliquid quia praesentium vel. Ut vel commodi tenetur et est.', 'https://lorempixel.com/640/480/?54701', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(14, 8, 9, 2, 'Voluptas quod veniam molestiae eaque rerum amet cupiditate ratione.', 'Nisi id aut illum consequatur voluptatem enim a. Et accusantium autem praesentium molestiae sit explicabo blanditiis. Voluptatem qui eum doloremque voluptas. Quo voluptas maxime voluptate.', 'https://lorempixel.com/640/480/?29405', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(15, 7, 1, 3, 'Autem molestiae voluptatum dolore corrupti aut.', 'Rem excepturi maxime corrupti ab sit tenetur asperiores. Magni suscipit iusto sed perferendis sunt veritatis. Qui numquam eos fugiat.', 'https://lorempixel.com/640/480/?11468', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(16, 3, 2, 1, 'Aliquid tempora odio eum ipsam facilis et.', 'Sequi officiis laboriosam itaque omnis nobis magnam. Eum aut autem quidem quia rerum. Esse dolor illum ex nemo est.', 'https://lorempixel.com/640/480/?72780', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(17, 10, 8, 7, 'Sit cumque quo ab quis dicta rerum aspernatur.', 'Voluptatibus sapiente assumenda quisquam necessitatibus illum explicabo. Quasi hic et natus possimus et facilis similique. Voluptas omnis commodi tempora ut. Velit est corrupti nihil dolorem deserunt dolore.', 'https://lorempixel.com/640/480/?55758', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(18, 3, 2, 10, 'Amet corporis molestiae minima delectus magni iusto nemo eos.', 'Qui voluptatem rerum facere molestiae. Aut ducimus harum ut corporis. Architecto quia ratione et porro ut.', 'https://lorempixel.com/640/480/?28273', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(19, 8, 1, 4, 'Aut sed perspiciatis fuga qui.', 'Maiores eligendi quasi adipisci voluptates. In facilis expedita dolore aut consectetur dolores dolor quia. Est incidunt fuga ut eius sapiente. Vel voluptas dolor non consequatur nostrum.', 'https://lorempixel.com/640/480/?72938', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(20, 3, 6, 3, 'Qui sint quam aperiam voluptatem id sed quam laudantium.', 'Necessitatibus soluta facere et vel sint ipsum. Nulla laudantium enim rerum voluptatibus rerum mollitia distinctio. Id atque nostrum quibusdam autem temporibus adipisci ea omnis.', 'https://lorempixel.com/640/480/?90573', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(21, 6, 10, 1, 'Eaque sit sint rerum aut rerum quaerat.', 'Magnam quis debitis ducimus non numquam. Aperiam veniam iusto consequatur corrupti. Iure ut eligendi nam minima consequatur quia aperiam. Doloremque nobis id consequatur vero quibusdam nihil nulla.', 'https://lorempixel.com/640/480/?38908', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(22, 9, 8, 2, 'Voluptates qui voluptas nobis hic.', 'Neque et pariatur totam rerum. Minus omnis tempore dignissimos harum. Reiciendis laudantium exercitationem dolorem suscipit molestiae. Magni dolores non nisi vel.', 'https://lorempixel.com/640/480/?78934', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(23, 2, 9, 3, 'Eos et rerum ad magnam voluptatem.', 'Voluptate asperiores est eum dolorum. Qui voluptatibus dolorem voluptate dicta. Et distinctio ut ut sint ex earum. Ducimus ut fuga aut. Vel consequuntur sunt reprehenderit accusantium mollitia.', 'https://lorempixel.com/640/480/?22648', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(24, 8, 10, 6, 'Tempore exercitationem cupiditate est iusto deserunt.', 'Consequatur sint eius corporis doloribus in minus. Aliquid non id dicta at corporis cumque. Similique est in possimus itaque.', 'https://lorempixel.com/640/480/?43655', '2020-08-22 00:04:53', '2020-08-22 00:04:53'),
(25, 2, 1, 9, 'Illo distinctio non voluptatem possimus voluptatem.', 'Perferendis id iste ut asperiores. Labore vitae omnis quod aut commodi et. Atque possimus veniam provident et eos eum fugiat. Quasi culpa suscipit quis reprehenderit aliquam.', 'https://lorempixel.com/640/480/?86533', '2020-08-22 00:04:53', '2020-08-22 00:04:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Prof. Elroy Bauch DVM', 'thea05@example.org', '2020-08-22 00:04:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wsRqeWru8j', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(2, 'Dr. Jamaal Mayer IV', 'sydni11@example.org', '2020-08-22 00:04:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CxaMJUaeYm', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(3, 'Colleen Johns', 'camille28@example.org', '2020-08-22 00:04:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YQzqM34w6F', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(4, 'Ellen Bednar', 'nelda08@example.org', '2020-08-22 00:04:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DGkhPCv8jO', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(5, 'Ahmed Schoen', 'crutherford@example.net', '2020-08-22 00:04:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aAZonj85BY', '2020-08-22 00:04:56', '2020-08-22 00:04:56'),
(6, 'Tareq  Ahmed', 'tareq@gmail.com', NULL, '$2y$10$L0C6YHxG9sRFwa/yrM63qexA2bZ8AoZ7yPfT.h2nJf84Z1GfRdq/u', NULL, '2020-08-22 00:05:15', '2020-08-22 00:05:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
